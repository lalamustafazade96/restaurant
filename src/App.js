import {Fragment} from "react";
import {Route} from "react-router-dom";
import Orders from "./components/Orders";
import OrderDetail from "./components/OrderDetail";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
      <Fragment>
          <Route path='/' exact>
              <Orders/>
          </Route>
          <Route path="/orders/:orderId/order-details" >
              <OrderDetail/>
          </Route>
    </Fragment>
  );
}

export default App;
