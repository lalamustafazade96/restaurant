import React, {useEffect, useState} from "react";
import {Button, Form, InputGroup, Modal} from "react-bootstrap";
import axios from "axios";

axios.defaults.baseURL = 'https://1dd484dc-c33b-4804-8c56-eb01a1fcdc01.mock.pstmn.io';
const NewItemPopup = (props) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [foodList, setFoodList] = useState([]);
    const [foodPrice, setFoodPrice] = useState(0);
    const [food, setFood] = useState({});
    const [count, setCount] = useState(0);

    useEffect(() => {
        axios.get('/items').then(
            (response => {
                setFoodList(response.data);
            })
        );
    }, []);

    const createDetail = () => {
        axios.post(`/orders/${props.orderId}/details`, {
            food,
            count
        })
            .then(response => {
                    setShow(false);
                    props.changeTrigger();
                }
            )
            .catch(error => console.log(error));
    };

    const handleChange = (e) => {
        const myFoodObj = foodList.find(obj => obj.id == e.target.value);
        setFoodPrice(myFoodObj.price);
        setFood(myFoodObj.id);
    }

    const handleCountChange = (e) => {
        setCount(e.target.value);
    }

    return (
        <div className='d-inline-block'>
            <Button variant="primary" onClick={handleShow}>
                Yemekleri elave et
            </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Yemek siyahisi </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Select className='mb-4' onChange={handleChange}>
                            <option defaultValue disabled>Yemek siyahisi</option>
                            {foodList.map(food => (
                                <option key={food.id} value={food.id}>{food.name}</option>
                            ))}
                        </Form.Select>
                        <Form.Label htmlFor="basic-url">qiymeti</Form.Label>
                        <InputGroup className="mb-3">
                            <Form.Control
                                type="text"
                                disabled
                                readOnly
                                value={foodPrice}
                            />
                        </InputGroup>
                        <Form.Label htmlFor="basic-url">sayi</Form.Label>
                        <InputGroup className="mb-3">
                            <Form.Control
                                type="number"
                                min='1'
                                onChange={handleCountChange}
                            />
                        </InputGroup>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={createDetail}>
                        Sifaris yarat
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}
export default NewItemPopup;