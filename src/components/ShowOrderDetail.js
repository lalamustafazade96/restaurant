import React, {useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";

const ShowOrderDetail = (props) => {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <div>
            <Button variant="primary" onClick={handleShow}>
                Bax
            </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ul className='foodList'>
                        <li>
                            <h3>{props.name}test</h3>
                            <div>miqdari:{props.count}</div>
                            <div>qiymeti:{props.price}</div>
                        </li>
                        <hr/>
                        <li>
                            <h3>{props.name}test</h3>
                            <div>miqdari:{props.count}</div>
                            <div>qiymeti:{props.price}</div>
                        </li>
                    </ul>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default ShowOrderDetail;