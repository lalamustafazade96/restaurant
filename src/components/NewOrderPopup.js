import React, {useEffect, useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import axios from "axios";
import {useHistory} from "react-router-dom";

axios.defaults.baseURL = 'https://1dd484dc-c33b-4804-8c56-eb01a1fcdc01.mock.pstmn.io';
const NewOrderPopup = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [waiterList, setWaiterList] = useState([]);
    const [tableList, setTableList] = useState([]);
    const [table, setTable] = useState({});
    const [waiter, setWaiter] = useState({});
    const history = useHistory();
    useEffect(() => {
        axios.get('/waiters').then(response => {
            setWaiterList(response.data);
        });
    }, []);

    useEffect(() => {
        axios.get('/tables').then(response => {
            setTableList(response.data);
        });
    }, []);
    const selectTable = (e) => {
        setTable({id: e.target.value});
    };
    const selectWaiter = (e) => {
        setWaiter({id: e.target.value});
    };
    const createOrder = () => {
        axios.post('/orders', {waiter, table})
            .then(response => {
                    history.push(`orders/${response.data.id}/order-details`);
                    setShow(false);
                }
            )
            .catch(error => console.log(error));
    };


    return (
        <div>
            <Button variant="primary" onClick={handleShow}>
                Yeni Sifaris
            </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Modal heading </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Select aria-label="Default select example" className='mb-4' onChange={selectTable}>
                            <option selected disabled>Masa</option>
                            {tableList.map(table => (
                                <option key={table.id} value={table.id}>{table.name}</option>
                            ))}
                        </Form.Select>
                        <Form.Select aria-label="Default select example" onChange={selectWaiter}>
                            <option selected disabled>Xidmetci</option>
                            {waiterList.map(waiter => (
                                <option key={waiter.id} value={waiter.id}>{waiter.name}</option>
                            ))}
                        </Form.Select>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={createOrder}>
                        Sifaris yarat
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}
export default NewOrderPopup;