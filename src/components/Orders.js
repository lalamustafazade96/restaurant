import React, {useEffect, useState} from "react";
import {Button, Container, Table} from "react-bootstrap";
import {useHistory} from "react-router-dom";
import axios from "axios";
import NewOrderPopup from "./NewOrderPopup";
import '../assets/scss/main.scss';

axios.defaults.baseURL = 'https://1dd484dc-c33b-4804-8c56-eb01a1fcdc01.mock.pstmn.io';
const Orders = () => {
    const [orderList, setOrderList] = useState([]);
    var i = 0;
    var amount = 0;
    var orderId;
    const history = useHistory();
    useEffect(() => {
        axios.get('/orders')
            .then(response => {
                setOrderList(response.data);
            });
    }, []);
    const endedOrder = (e) => {
        const id = e.target.value;
        axios.patch(`/orders/${id}/status`, {kod: 2})
            .then(response => {
                e.target.disabled = true;
                alert('mock serves oldugu ucun deyisiklik listde eks olunmur');
            })
    }

    const deleteOrder = (e) => {
        const id = e.target.value;
        axios.patch(`/orders/${id}/status`, {kod: 3})
            .then(response => {
                e.target.disabled = true;
                alert('mock serves oldugu ucun deyisiklik listde eks olunmur');
            });
    }
    const watchOrderDetails = (e) => {
        orderId = e.target.value;
        history.push(`orders/${orderId}/order-details`);
    }
    return <Container id="c1">
        <div className='d-flex justify-content-between align-items-center flex-wrap my-5'>
            <div>
                <h3>SIFARISLERIN QEBULU</h3>
            </div>
            <div>
                <NewOrderPopup/>
            </div>
        </div>
        <Table id="table1" striped bordered size="sm" responsive="sm" className='my-5'>
            <thead>
            <tr>
                <th>#</th>
                <th>Masa</th>
                <th>Xidmetci</th>
                <th>Status</th>
                <th>Mebleg</th>
                <th>Baslama tarixi</th>
                <th>Sonlanma tarixi</th>
                <th>Etrafli</th>
                <th>Statusu deyis</th>
            </tr>
            </thead>
            <tbody>
            {orderList.map((order) => {
                if (order.status.kod === 1) {
                    amount += order.amount
                }
                return (<tr key={order.id} value={order.id}>
                    <td>{i++}</td>
                    <td>{order.table.name}</td>
                    <td>{order.waiter.name}</td>
                    <td className={`${order.status.kod === 1 ? 'red-status' : ''} `}>{order.status.name}</td>
                    <td>{order.amount}</td>
                    <td>{order.createDate}</td>
                    <td>{order.endDate}</td>
                    {/*<td><Link to={`/orders/${orderId}/order-details`}><Button value={order.id} onClick={watchOrderDetails}>Bax</Button></Link></td>*/}
                    <td className='text-center'><Button value={order.id} onClick={watchOrderDetails}>Bax</Button></td>
                    <td className='text-center'>
                        <Button className='btn-info mx-2' onClick={endedOrder} value={order.id}>Sonlandir</Button>
                        <Button className='btn-danger mx-2' onClick={deleteOrder} value={order.id}>Legv et</Button>
                    </td>
                </tr>)

            })}
            <tr>
                <td colSpan='4'><h4>Cemi mebleg:</h4></td>
                <td colSpan='5'><h4>{amount} AZN</h4></td>
            </tr>
            </tbody>
        </Table>
    </Container>
}
export default Orders;