import React, {useEffect, useState} from "react";
import {Button, Container, Table} from "react-bootstrap";
import NewItemPopup from "./NewItemPopup";
import {Link, useParams} from "react-router-dom";
import axios from "axios";

axios.defaults.baseURL = 'https://1dd484dc-c33b-4804-8c56-eb01a1fcdc01.mock.pstmn.io';
const OrderDetail = () => {
    const [orderDetails, setOrderDetails] = useState([]);
    const [order, setOrder] = useState({});
    const [trigger, setTrigger] = useState(0);
    const {orderId} = useParams();
    var totalAmount = 0;

    useEffect(() => {
        axios.get(`/orders/${orderId}/details`).then(
            response => {
                setOrderDetails(response.data);
            }
        );
    }, [orderId,trigger]);

    useEffect(() => {
        axios.get(`/orders/${orderId}`).then(
            response => {
                setOrder(response.data);
            }
        );
    }, [orderId]);

    const changeTrigger = () => {
        setTrigger(trigger + 1);
    }
    const finishOrderStatus = (e) => {
        const id = e.target.value;
        axios.patch(`/orders/${orderId}/details/${id}/status`, {kod: 2})
            .then(response => {
                e.target.disabled = true;
                alert('mock serves oldugu ucun deyisiklik listde eks olunmur');
            });
    }
    const deleteOrderStatus = (e) => {
        const id = e.target.value;
        axios.patch(`/orders/${orderId}/details/${id}/status`, {kod: 3})
            .then(response => {
                e.target.disabled = true;
                alert('mock serves oldugu ucun deyisiklik listde eks olunmur');
            })
    }
    return (
        <Container className='mt-5'>
            <div className='d-flex flex-wrap justify-content-between my-5'>
                <div><h4 className='text-success'>Masa : {order.table?.name}</h4></div>
                <div><h4 className='text-success'>Xidmetci : {order.waiter?.name}</h4></div>
                <div>
                    <NewItemPopup orderId={orderId} changeTrigger={changeTrigger}/>
                    <Link to='/' style={{textDecoration: 'none'}}><Button className='btn-info d-inline mx-3'>Esas
                        sehife</Button></Link>
                </div>
            </div>
            <Table striped bordered size="sm" responsive="sm">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Mehsul Adi</th>
                    <th>Miqdar</th>
                    <th>Qiymet</th>
                    <th>Mebleg</th>
                    <th>Sifaris Saati</th>
                    <th>Gozleme</th>
                    <th>Verildi</th>
                    <th>Geri</th>
                </tr>
                {orderDetails.map((data, index) => {
                        totalAmount += data.item.price * data.count;
                        return (<tr key={data.id}>
                            <td>{index + 1}</td>
                            <td>{data.item.name}</td>
                            <td>{data.count}</td>
                            <td>{data.item.price}</td>
                            <td>{data.item.price * data.count}</td>
                            <td>{data.orderDate}</td>
                            <td>{data.itemStatus.statusName}</td>
                            <td className='text-center'><Button className='btn-success' value={data.id}
                                                                onClick={finishOrderStatus}>Tehvil
                                verildi</Button></td>
                            <td className='text-center'><Button className='btn-danger' value={data.id}
                                                                onClick={deleteOrderStatus}>Imtina
                                edildi</Button></td>
                        </tr>)
                    }
                )}
                </thead>
                <tbody>
                <tr>
                    <td colSpan='4'><h4>Cemi Mebleg</h4></td>
                    <td colSpan='5'><h4>{totalAmount} AZN</h4></td>

                </tr>
                </tbody>
            </Table>
        </Container>
    )
}
export default OrderDetail;